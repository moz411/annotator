import React from "react";
import saveAs from "file-saver";
import DocViewer, { DocViewerRenderers } from "react-doc-viewer";

import {
    Document,
    Packer  
} from "docx";

const docs = [{uri: "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,"}];

export default function RenderDocument({pdfData}) {

    function saveFile() {
    // console.log(pdfData)
    const doc = new Document({
        sections: pdfData
    });
    Packer.toBlob(doc).then((blob) => {
        // saveAs from FileSaver will download the file
        saveAs(blob, "example.docx");
        // docs.push({uri: "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document.document;base64," + string});
    });
}

    return (
    // <DocViewer documents={docs} />
        <div>
            <div className="flex-container">
                <div id="content" className="content">
                    <button className="cv" onClick={saveFile}>
                        Download File
                    </button>
                </div>
            </div>
        </div>
        )
}