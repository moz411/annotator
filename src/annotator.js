import React, { useState } from "react";
import DocumentBackend from "./CreateDocument.js";
import PDFJSWorker from "worker-loader!pdfjs-dist/build/pdf.worker.js";
const pdfjsLib = require("pdfjs-dist");
pdfjsLib.GlobalWorkerOptions.workerPort = new PDFJSWorker();
import {
  Document,
  Packer, 
  Paragraph,
  TextRun,
  ImageRun,
  FrameAnchorType,
  FrameWrap,
  HorizontalPositionRelativeFrom,
  VerticalPositionRelativeFrom
} from "docx";
import saveAs from "file-saver";
import testImage from "./image-000.png";

const docFrames = [];


export default function PDFJSBackend() {

  const reader = new FileReader();
  const [isLoaded, setIsLoaded] = useState(false);
  const extractedImage = 'iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=='
  const images = [];
  // PDF to twip coordinates
  function convertPDFtoTwip(coord) {
    return coord * 20
  };

  reader.onload = function () {
    var pdfData = new Uint8Array(this.result);

    // Loading a document.
    pdfjsLib.getDocument({ data: pdfData }).promise
      .then(pdfDocument => {
        for (let i = 1; i < pdfDocument.numPages + 1; i++) {
          pdfDocument.getPage(i)
            .then(pdfPage => {
              // Getting text
              var res = getText(pdfPage, []);

              images.push(
                <div className="flex-container">
                  <img src={testImage} />
                </div>
              );
              // Getting images
              // res = getImages(pdfPage, res);
              // docFrames.push({ children: res });
            });
        }
      })
      .catch((reason) => {
        console.error("Error: " + reason);
      });
  };

  // retreive text in page
  function getText(pdfPage, result) {
    pdfPage.getTextContent()
      .then(textContent => {
        var prev_x, prev_y, text;
        for (let j = 0; j < textContent.items.length; j++) {
          var element = textContent.items[j];
          var x = convertPDFtoTwip(element.transform[4]);
          var y = convertPDFtoTwip(pdfPage.view[3] - element.transform[5]);
          var width = convertPDFtoTwip(element.width);
          var height = convertPDFtoTwip(element.height);
          // console.log(x, y, prev_x, prev_y, width);
          // if text boxes are adjacent, join them
          // if ((Math.abs(x - prev_x + width) < 300) && (y - prev_y < 100) ) {
          //   text += element.str;
          //   console.log(text);
          //   continue;
          // } 
          text = element.str;
          prev_x = x;
          prev_y = y;

          // console.log(x, y, width, height);
          result.push(new Paragraph({
            frame: {
              position: {
                x: x,
                y: y,
              },
              width: width,
              height: height,
              anchor: {
                horizontal: FrameAnchorType.PAGE,
                vertical: FrameAnchorType.PAGE,
              },
              wrap: {
                type: FrameWrap.NONE,
              },
              alignment: {
                x: HorizontalPositionRelativeFrom.PAGE,
                y: VerticalPositionRelativeFrom.PAGE,
              },
            },
            children: [
              new TextRun({
                text: text,
                font: element.fontName,
                size: Math.round(element.height * 2)
              }),
            ]
          }));
        }
      });
    return result;
  };

  // retreive images in page
  function getImages(pdfPage, result) {
    pdfPage.getOperatorList().then((ops) => {

      // const viewport = pdfPage.getViewport({ scale: 1.0 });
      // const svgGfx = new pdfjsLib.SVGGraphics(pdfPage.commonObjs, pdfPage.objs);
      // svgGfx.getSVG(ops, viewport).then(function (svg) {
      //   window.svgImage = svg;
      //   console.log(window.svgImage);
      // });

      for (var j = 0; j < ops.fnArray.length; j++) {
        if (ops.fnArray[j] == pdfjsLib.OPS.paintImageXObject) {

          var key = ops.argsArray[j]
          var image = pdfPage.objs._objs[key[0]].data;
          // console.log(image)
          var transform = ops.argsArray[j - 2];
          var x = convertPDFtoTwip(transform[4]);
          var y = convertPDFtoTwip(pdfPage.view[3] - transform[5]);
          var width = convertPDFtoTwip(image.width);
          var height = convertPDFtoTwip(image.height);

          result.push(new ImageRun({
            data: [image.data],
            transformation: {
              width: width,
              height: height,
              floating: {
                horizontalPosition: {
                  offset: x,
                  relative: HorizontalPositionRelativeFrom.PAGE
                },
                verticalPosition: {
                  offset: y,
                  relative: VerticalPositionRelativeFrom.PAGE
                },
              },
            }
          }));
        }
        // if (ops.fnArray[j] == pdfjsLib.OPS.paintJpegXObject) {
        //   images.push(pdfPage.objs[ops.argsArray[j])
        //   // console.log("paintJpegXObject");
        // }
        // if (ops.fnArray[j] == pdfjsLib.OPS.paintInlineImageXObject) {
        //   images.push(pdfPage.objs[ops.argsArray[j])
        //   // console.log("paintInlineImageXObject");
        // }
        // if (ops.fnArray[j] == pdfjsLib.OPS.paintXObject) {
        //   images.push(pdfPage.objs[ops.argsArray[j])
        //   // console.log("paintXObject");
        // }
      }
    });
    return result;
  };

  // Load pdf file
  function onLoadPdf(evt) {
    reader.readAsArrayBuffer(evt.target.files[0]);
    setIsLoaded(!isLoaded);
  };

  

  function logInfo(e) {
    console.log(e)
  }

  return isLoaded ? (
    <div>
      <div className="flex-container">
        <div id="content" className="content">
          <button className="cv" onClick={saveFile}>
            Download File
          </button>
        </div>
      </div>
      <div className="flex-container">
        {images}
      </div>
    </div>
  ) : (
    <DocumentBackend />
  )
}